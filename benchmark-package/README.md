Copyright 2022-2023 Arm Limited (or its affiliates). All rights reserved.

# Arm IP Explorer custom software development example

This software is for [Arm IP Explorer](https://ipexplorer.arm.com)

You can prepare software applications for benchmarking on Arm processors and run them with IP Explorer.

You need an Arm account to access IP Explorer. If you need a new account visit [Arm Developer](https://developer.arm.com) and click the person icon in the top right corner and click Register. If you don't have access to IP Explorer you will get a form to request access. Fill out the form and somebody will review and grant access.

This software is compatible with a Linux platform where Python 3.8 is installed. It has been tested against Centos version 7.9.2009 and Python 3.8.2 but should work on other Unix compliant operating systems. It has not been tested with cygwin or WSL on Windows and is not guaranteed to work with those. When building a CMSIS application, the Python module `pyparsing` is also required; it has been tested against pyparsing 3.0.8.  If IP Explorer is used to compile applications, all you need is a text editor.  If you plan on compiling applications on premises, the ARM Compiler for Embedded (<https://developer.arm.com/Tools%20and%20Software/Arm%20Compiler%20for%20Embedded>) and/or the ARM GNU Toolchain (<https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads>) must be installed.

For more information on the software in this package, see the [Reference](#reference) section later in this file.  The system-specific README.txt file mentioned in that section supplies the exact compiler versions supported by each system.

## Create and upload custom software applications

The steps below teach you how to create a basic software application for a Cortex-M system. Once you learn them you can create more complex applications for benchmarking.

It helps if you have a compiler for cross-compiling for Cortex-M.

The executable for the ARM GNU Toolchain is `arm-none-eabi-gcc`.

If you don't have it, IP Explorer can compile your applications, but it saves time if you check them yourself first to save time uploading.

## Get started

### Step 0

Log in to IP Explorer and click `Simulate IP` on the top right navigation in Arm IP Explorer.

Click an existing project under `Select Project`.

Click `Cortex-M0Plus` under `Select System`.

Download the software source code by scrolling to the bottom of the page and clicking `Download Software`.

Copy downloaded file `user-software.tgz` to a directory where you want to work.

Extract the download:

```console
tar xf user-software.tgz
```

Everything is located in the `benchmark-package` directory:

```console
cd benchmark-package
```

### Step 1

Compile one of the provided applications or create your own and compile it.

To build the "hello world" application which is provided in the download using the GCC compiler run:

```bash
bash ./build_app.sh hello m0px1_nocache GCC
```

If you chose to create your own application, it must be built using the provided build_app.sh script to ensure the resultant executable will run with the specified system.

To create a new application called `fp-multiply` use:

```bash
cp -r Applications/app-template Applications/fp-multiply
mv Applications/fp-multiply/app-template.c Applications/fp-multiply/fp-multiply.c
```

### Step 2

Edit the file `sw_options.json` and set `software_name`, `software_description`, `software_version`  and `valid_systems` tags.

The `software_name` tag value should have the name of all applications you would like to upload. For example: `"software_name": ["fp-multiply"],`. For each application provide the software version.
For example: `"software_version": ["1.0"],`.

The `valid_systems` tag value should have the system names you want to run your custom software on. For example: `"valid_systems": ["m0px1_nocache"].`

The Systems directory contains the names of the systems that are options for the `"valid_systems"`.

```json
{
        "software_name": ["fp-multiply"],
        "software_description": ["Floating point multiply test"],
        "software_version": ["1.0"],
        "valid_compilers": ["AC6","GCC"],
        "valid_systems": ["m0px1_nocache"]
}
```

Save changes in the json file.

To create multiple applications, repeat this step for each application.

### Step 3

Modify the `Applications/fp-multiply/fp-multiply.c` source file to add your benchmark code between the comments "Enter user defined code here" and "End user defined code here" and save changes. For example, use this `main()` function:

```c
int main()
{
    double a = 3.14;
    double b = 6.023456;
    double product;

    (void) start_marker();

    // Calculating product
    product = a * b;

    (void) stop_marker();

    printf("Product = %.2lf\n", product);

    return 0;
}
```

### Step 4 (optional)

You can compile your custom software before uploading it.

You can also let IP Explorer compile your software after upload. If there are any errors it is more time-consuming to skip the local compilation.

To compile the `fp-multiply` example application with the Arm GNU toolchain:

```bash
bash ./build_app.sh fp-multiply m0px1_nocache GCC
```

This step creates the compiled software executable in ./Applications/fp-multiply/GCC/fp-multiply.axf

It's also possible to not share your source code with IP Explorer and just provide binary files to run. Look in the [Reference](#reference) section for the `"cloud_compilation"` option which, when set to `"false"`, tells IP Explorer you don't want it to do any compiling. The default is to compile your source code in IP Explorer.

### Step 5

Verify the `sw_options.json` file.

```bash
./app_checker.py
```

### Step 6

Package the source tree for upload to IP Explorer:

```bash
./app_checker.py -t
```

Upload the `custom-software.tgz` created in the parent directory to Arm IP Explorer and run it on the available systems.

## Reference

This section provides more information on the downloaded `user-software.tgz` file.  The contents of this file are known as the user software package.

The user software package contains the source code to build a variety of applications for IP Explorer systems
using supported compilers.  Some applications are provided pre-built.

The user software package becomes a custom software package after custom applications are added.  This package is typically named `custom-software.tgz`.

### Directory Structure

Extracting the user software package by doing `tar xf ../user-software.tgz` creates a directory named `benchmark-package` with these contents:

```
./README.md                This file.
./data.json                Indicates the version of the user software package.
./sw_options.json          Example software options file that was edited above.
./build_app.sh             Helper bash script to build applications in this package.
./app_checker.py           Helper python script to help users (you!) validate that your custom
                           application has the required files and builds correctly. See
                           below for more details.
./Applications/<app>       Directory containing the source code and Makefile for a standard application <app> supported
                           by the user software package.  This directory also contains the app-template that is copied to create a custom application.
./Scripts/                 Directory containing code used by the helper scripts.
./Systems/<sysid>          Directory containing system-specific source, build, and README files needed to build an
                           application for the system <sysid>.  Built application
                           tarballs are also found here at `<sysid>/<app>/<compiler>/<app>_<compiler>.tgz`, where <compiler>
                           is either `AC6` (ARM Compiler for Embedded) or `GCC` (ARM GNU Toolchain).  The mapping of the
                           <sysid> directories to IP Explorer system names is:
          a32x4_nic-400/          Cortex-A32
          a34x4_nic-400/          Cortex-A34
          a35x4_nic-400/          Cortex-A35
          a53x2_nic-400_32kb/     Cortex-A53 with 32kb I/D Caches
          a53x2_nic-400_64kb/     Cortex-A53 with 64kb I/D Caches
          a55_nic400_cci_500/     Cortex-A55
          a5x4_nic-400/           Cortex-A5
          a7x4_nic-400/           Cortex-A7
          m0px1_cache/            Cortex-M0Plus with AHB-Cache
          m0px1_nocache/          Cortex-M0Plus
          m0x1_cache/             Cortex-M0 with AHB-Cache
          m0x1_nocache/           Cortex-M0
          m23x1-axi4/             Cortex-M23
          m33x1-axi4/             Cortex-M33 with FPU
          m3x1-axi4/              Cortex-M3
          m4x1_cache_2kb/         Cortex-M4 with 2kb AHB-Cache
          m4x1_cache_64kb/        Cortex-M4 with 64kb AHB-Cache
          m4x1_nocache_ws0/       Cortex-M4 with 0 Wait-State SRAM
          m4x1_nocache_ws4/       Cortex-M4 with 4-cycle Wait-State SRAM
          m52x1-axi4/             Cortex-M52 with FPU
          m55x1-axi4/             Cortex-M55 with FPU
          m7x1-axi4/              Cortex-M7 with FPU
          m85x1-axi4/             Cortex-M85 with FPU
          r52x1_nic-400/          Cortex-R52
          r5x1_nic-400/           Cortex-R5
          r82x1-axi4/             Cortex-R82
          r8x4_nic-400/           Cortex-R8
                            When calling scripts or filling out the `sw_options.json` file,
                            please use only the <sysid>s (the column at the left above).
./Systems/<sysid>/README.txt README describing supported applications, compilers, and memory
                           map for system <sysid>.
./license_terms/           Directory containing Arm and third party license files
```

### Guidelines

This package enables you to write and build custom applications for a variety of systems using the existing standard applications as examples and/or starting places.

This package supports:

- The systems in `./benchmark-package/Systems`.
- The standard applications in `./benchmark-package/Applications`.
- Custom applications you create in `./benchmark-package/Applications`.
- Building applications with ARM Compiler for Embedded (indicated by `AC6`) and/or ARM GNU Toolchain (indicated by `GCC`).

For each system `<sysid>`, the supported standard applications, build tool versions, and the memory maps through which applications interface to the system are specified in `benchmark-package/Systems/<sysid>/README.txt`.

The package contains all of the code to boot and run applications for each system.  We recommend that you do not modify any files in the `Systems` directory. These files have system-specific code in them that, if changed, could prevent the system or application from running properly.

### Files To Edit

This section discusses files you will need to edit when creating a custom application.

#### Application Template

`./benchmark-package/Applications/app-template` contains these files that are copied to create a custom application named `<app>`:

- app-template.c - a minimal C source file that marks an empty software region; to use this file as a starting point, copy it to  `./benchmark-package/Applications/<app>/<app>.c`.
- Makefile - a makefile that can build an application for a specified system with either `AC6` or `GCC`; to use it to build a custom application, copy it to `./benchmark-package/Applications/<app>/Makefile`.

Some systems allow applications to be built with or without including FPU instructions.  To not use FPU instructions, the application Makefile must explicitly assign `NO_FPU=1`.  When this is done the application Makefile will not use FPU compiler and linker flags and the application will not initialize/enable the FPU or any associated Coprocessor registers via the Coprocessor Access Control Register.  See the individual `./benchmark-package/Systems/<sysid>/Source/makefile-system.inc` files to determine which systems support use of this `NO_FPU` define.

If you have more than one custom application, use the files in `app-template` for each one.

#### Software Options

The `sw_options.json` file is used by IP Explorer when the custom software package is uploaded.  It indicates the software applications to build, where the builds will occur, and the systems where the applications will run.  This JSON file contains 4 required and 1 optional tag:

- `software_name` is an array of the custom applications you would like to upload. For example: `"software_name": ["myapp1", "myapp2"]`.  These names should match the values of `<app>` used in the previous section.

- `software_description` is an array giving a comment describing each custom application.  The array elements for the value of `software_description` correspond to those for the value of `software_name`.  For example: `"software_descripton": ["My first custom app.", "My second custom app."]`.

- `software_version` is an array of the custom application versions to be to uploaded. For example: `"software_version": ["1.0", "2.0"]`.

- `valid_compilers` is an array of at most two elements: `"AC6"` and/or `"GCC"`.  For example: `"valid_compilers": ["AC6", "GCC"]`.

- `valid_systems` is an array giving the `<sysid>` of each system where your custom software will run. For example: `"valid_systems": ["m7x1-axi4", "m55x1-axi4"]`.

- `cloud_compilation` is an optional tag. It controls whether custom applications are compiled in the cloud or pre-built locally.  There is one setting that applies to all custom applications.  It has the boolean value `true` or `false`, with `true` being the default if it is unset. For example: `"cloud_compilation":true` results in the compiling of all custom applications in the cloud.

NOTE: When compiling in the cloud, you must supply source code.

If you don't want to upload source code, it's possible to instead supply pre-built versions of each application.
To do so, perform the following additional actions:

- Set `"cloud_compilation": false` in the `sw_options.json` file.
- Add a `compiler_version` section to `sw_options.json`. This is an array of the same length as `valid_compilers`. Each
  element in this array represents the version of the corresponding compiler used when compiling the application. These
  version numbers may be any valid string, and are meant to enable easier tracking of custom software packages in a project.
- Pre-build the application for all combinations of `<app>` in `software_name`, `<sysid>` in `valid_systems`, and `<compiler>`
  in `valid_compilers` using either `build_app.sh` or `app_checker.py`.=
- Each pre-built application tarball should be located at
  `./benchmark-packages/Systems/<sysid>/<app>/<compiler>/<app>_<compiler>.tgz`

The examples used for this step result in this `sw_options.json` file when cloud compilation is enabled:

```json
{
    "software_name": ["myapp1", "myapp2"],
    "software_description": ["My first custom app.", "My second custom app."],
    "software_version": ["first app version", "second app version"],
    "valid_compilers": ["AC6","GCC"],
    "valid_systems": ["m7x1-axi4", "m55x1-axi4"],
    "cloud_compilation": true
}
```

When pre-building applications, the `sw_options.json` file would look like this:

```json
{
    "software_name": ["myapp1", "myapp2"],
    "software_description": ["My first custom app.", "My second custom app."],
    "software_version": ["first app version", "second app version"],
    "valid_compilers": ["AC6","GCC"],
    "compiler_version": ["6.19", "11.3.rel1"],
    "valid_systems": ["m7x1-axi4", "m55x1-axi4"],
    "cloud_compilation": false
}
```

Note the value of `cloud_compilation` as `false` and ordering of the `compiler_version` array. This indicates the applications
were pre-built using `AC6` version `6.19` and `GCC` version `11.3.rel1`.

### Scripts To Run

This section discusses the scripts you will need to run when preparing a custom application.

#### build_app.sh

To compile application source locally on your machine, make sure you have the compiler installed, your `pwd` is `./benchmark-package`, and then run:

```bash
bash ./build_app.sh <app> <sysid> <compiler>
```

The `build_app.sh` script takes an application name (`<app>`), a system (`<sysid>)`, and a compiler id (`GCC` or `AC6`).  `<app>` must identify a sub-directory of `./benchmark-package/Applications` and `<sysid>` must identify a subdirectory of `./benchmark-package/Systems`.

This step builds the software and upon success creates the application tarball `./benchmark-packages/Systems/<sysid>/<app>/<compiler>/<app>_<compiler>.tgz`.

If you are creating multiple applications, targeting multiple systems, or using multiple compilers you need to run `bash ./build_app.sh` one time for each combination.

#### app_checker.py

```bash
./app_checker.py -h
usage: app_checker.py [-h] [-b] [-t] [sw_options_file]

Application Checker

positional arguments:
  sw_options_file  Defaults to sw_options.json if not specified

optional arguments:
  -h, --help       show this help message and exit
  -b, --build      Build the applications
  -t, --tar        Tar up the software-package directory
```

The `app_checker.py` script is run for one of 3 purposes based on the options supplied:

- no options: checks that the `sw_options.json` file specifies correct application(s), system(s) and compiler(s).  Also, when not compiling in the cloud, verifies that the referenced application tarballs exist.
- `-b`: builds the applications as indicated by the `sw_options.json` file by calling `build_app.sh` repeatedly.
- `-t`: creates a `custom-software.tgz` file for upload.  When not compiling in the cloud, exclude from this the application source files.

### Upload

Upon upload of a `custom-software.tgz`, these things happen:

- each application is built for each targeted system with each specified compiler (when `"cloud_compilation": true`).
- each built application is run in a Fast Model simulation on the targeted system.

The building step is done only when cloud compilation is indicated.  If it isn't, the `custom-software.tgz` must contain the prebuilt application tarballs defined in [this section](#build_appsh).  The simulation step is to validate that a built application will run to completion on a quick Fast Model simulation.  It is important to establish success with this instruction-accurate simulation run before proceeding to the more time-consuming, resource-intensive cycle-accurate simulation that is done when a custom application is selected for running in IP Explorer.

#### Example

For the multiple application, multiple system example shown in the [Software Options](#software-options) section above, there are 8 distinct `<sysid>, <app>, <compiler>` compilations done in the cloud:

```
  m7x1-axi4, myapp1, AC6
  m7x1-axi4, myapp1, GCC
  m7x1-axi4, myapp2, AC6
  m7x1-axi4, myapp2, GCC
  m55x1-axi4, myapp1, AC6
  m55x1-axi4, myapp1, GCC
  m55x1-axi4, myapp2, AC6
  m55x1-axi4, myapp2, GCC
```

If these are successful, these compilations result in 8 application tarballs and are followed by 8 distinct Fast Model simulations.

#### Timeouts

To ensure that errant custom applications do not consume excessive compute resources, these limits are enforced:

- application builds must complete in less than 1 hour
- instruction-accurate Fast Model simulations must complete in less than 6 hours
- cycle-accurate simulations must complete in less than 24 hours

If any of these run longer than the specified time, they are terminated.
