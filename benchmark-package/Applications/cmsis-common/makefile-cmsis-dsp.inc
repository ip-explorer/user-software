#*********************************************************************
#       Copyright (c) 2022-2023 Arm Limited. All rights reserved.
#*********************************************************************

#########################################################################################
# Rules to build the application specific source files
#########################################################################################

# Include the system makefile
include ../../Systems/$(SYSTEM)/Source/makefile-system.inc

# This makefile includes just about everything needed to compile the CMSIS DSP
# benchmarks except the benchmark specific files. Those should manually be added with the makefile

#########################################################################################
# Update the defines, cflags, ldflags, and optimizations for the application build
#########################################################################################
FLOAT = hard
ifneq (,$(filter $(CMSIS_DEVICE),ARMCM23 ARMCM3 ARMCM0 ARMCM0plus))
FLOAT = soft
endif

# Shared defines between compilers
OPTIMIZATIONS += -g -Ofast -ffast-math
# CMSIS_DEVICE is defined and must be specified in the CPU makefile
DEFINES		  += -DNDEBUG -DEMBEDDED -DBENCHMARK -DCORTEXM
CFLAGS		  += -mfloat-abi=$(FLOAT) -Wimplicit-function-declaration
# LDFLAGS		  += something

## AC6
ifeq ($(COMPILER_TYPE),AC6)
# OPTIMIZATIONS   += something
# DEFINES		  += something
CFLAGS += -Wsign-compare -Wdouble-promotion
LDFLAGS += --userlibpath=$(COMPILER_DIR) --library=CMSISDSP

## GCC-RM
else ifeq ($(COMPILER_TYPE),GCC-RM)
# OPTIMIZATIONS   += something
# DEFINES		  += something
# We add "-fexceptions" because normally our GCC code compiles with "-fno-exceptions", however,
# the CMSIS benchmark has exceptions so we need to override the option
# Other options come from the CMSIS-DSP package
# Override LD with g++ in order to avoid undefined c++ function
LD     = arm-none-eabi-g++
CFLAGS += -fexceptions -flax-vector-conversions -mfp16-format=ieee
# We are using gcc to call ld so we need to manually specify libstdc++ and libmath
LDFLAGS += -mfloat-abi=$(FLOAT) -L$(COMPILER_DIR) --specs=nano.specs -lCMSISDSP -lstdc++ -lm -Wl,--gc-sections

## GCC-A-EABI
else ifeq ($(COMPILER_TYPE),GCC-A-EABI)
# OPTIMIZATIONS   += something
# DEFINES		  += something
# CFLAGS		  += something
# LDFLAGS		  += something

## GCC-A-ELF
else ifeq ($(COMPILER_TYPE),GCC-A-ELF)
# OPTIMIZATIONS   += something
# DEFINES		  += something
# CFLAGS		  += something
# LDFLAGS		  += something

else
$(error ERROR: COMPILER variable not valid. Must be either AC6, GCC. Currently: $(COMPILER) with COMPILER_TYPE set to $(COMPILER_TYPE))
endif

#########################################################################################
# Rules to build the CMSIS DSP library
#########################################################################################

CMSIS_5 = $(PWD)/CMSIS/CMSIS_5-5.9.0

# Path to CMSIS_DSP
CMSIS_DSP = $(PWD)/CMSIS/CMSIS-DSP-1.14.2

LIB_INCLUDES = \
	-I $(CMSIS_5)/CMSIS/Core/Include \
	-I $(CMSIS_DSP)/Include \
	-I $(CMSIS_DSP)/PrivateInclude

# Sources
LIB_SRCS = $(CMSIS_DSP)/Source/BasicMathFunctions/BasicMathFunctions.c \
	$(CMSIS_DSP)/Source/CommonTables/CommonTables.c \
	$(CMSIS_DSP)/Source/InterpolationFunctions/InterpolationFunctions.c \
	$(CMSIS_DSP)/Source/BayesFunctions/BayesFunctions.c \
	$(CMSIS_DSP)/Source/MatrixFunctions/MatrixFunctions.c \
	$(CMSIS_DSP)/Source/ComplexMathFunctions/ComplexMathFunctions.c \
	$(CMSIS_DSP)/Source/QuaternionMathFunctions/QuaternionMathFunctions.c \
	$(CMSIS_DSP)/Source/ControllerFunctions/ControllerFunctions.c \
	$(CMSIS_DSP)/Source/SVMFunctions/SVMFunctions.c \
	$(CMSIS_DSP)/Source/DistanceFunctions/DistanceFunctions.c \
	$(CMSIS_DSP)/Source/StatisticsFunctions/StatisticsFunctions.c \
	$(CMSIS_DSP)/Source/FastMathFunctions/FastMathFunctions.c \
	$(CMSIS_DSP)/Source/SupportFunctions/SupportFunctions.c \
	$(CMSIS_DSP)/Source/FilteringFunctions/FilteringFunctions.c \
	$(CMSIS_DSP)/Source/TransformFunctions/TransformFunctions.c

LIB_OBJECTS = $(patsubst $(CMSIS_DSP)/%.c,$(COMPILER_DIR)/%.o,$(LIB_SRCS))

# Build the .o files for libCMSISDSP.a
# We don't specifiy std=c11 at the top of the file because we do not want
# to apply it to the common C files like serial.c
$(COMPILER_DIR)/%.o: $(CMSIS_DSP)/%.c
	@mkdir -p $(@D)
	$(CC) -c $(OPTIMIZATIONS) $(CFLAGS) $(DEFINES) -std=c11 $(LIB_INCLUDES) $< -o $@

# Build the DSP library
$(COMPILER_DIR)/libCMSISDSP.a: $(LIB_OBJECTS)
	$(AR) -rc $@ $(LIB_OBJECTS)

#########################################################################################
# Rules to build the files for the benchmark application
#########################################################################################
# If CMSIS_DEVICE_INCLUDE not set then set it to the CMSIS_DEVICE
CMSIS_DEVICE_INCLUDE ?= $(CMSIS_DEVICE)

APP_INCLUDES = \
	-I $(CMSIS_5)/CMSIS/Core/Include \
	-I $(CMSIS_5)/Device/ARM/$(CMSIS_DEVICE_INCLUDE)/Include \
	-I $(CMSIS_DSP)/Include \
	-I $(CMSIS_DSP)/Testing/FrameworkInclude \
	-I $(CMSIS_DSP)/Testing/GeneratedInclude \
	-I $(CMSIS_DSP)/Testing/Include/Benchmarks

# Note that in the list below, TestDesc.cpp is generated for each benchmark
# but is always in the same location
# This list is appended by the application makefile to add benchmark specific sources
CMSIS_DSP_TESTDIR=$(CMSIS_DSP)/Testing
APP_SRCS = \
	$(CMSIS_DSP_TESTDIR)/main.cpp \
	$(CMSIS_DSP_TESTDIR)/testmain.cpp \
	$(CMSIS_DSP_TESTDIR)/FrameworkSource/ArrayMemory.cpp \
	$(CMSIS_DSP_TESTDIR)/FrameworkSource/Calibrate.cpp \
	$(CMSIS_DSP_TESTDIR)/FrameworkSource/Error.cpp \
	$(CMSIS_DSP_TESTDIR)/FrameworkSource/FPGA.cpp \
	$(CMSIS_DSP_TESTDIR)/FrameworkSource/Generators.cpp \
	$(CMSIS_DSP_TESTDIR)/FrameworkSource/Pattern.cpp \
	$(CMSIS_DSP_TESTDIR)/FrameworkSource/PatternMgr.cpp \
	$(CMSIS_DSP_TESTDIR)/FrameworkSource/Test.cpp \
	$(CMSIS_DSP_TESTDIR)/FrameworkSource/Timing.cpp \
	$(CMSIS_DSP_TESTDIR)/GeneratedSource/TestDesc.cpp

OBJ_FILES += $(patsubst $(CMSIS_DSP_TESTDIR)/%.cpp,$(COMPILER_DIR)/%.o,$(APP_SRCS))
OBJ_FILES += $(COMPILER_DIR)/patterndata.o
OBJ_FILES += $(COMPILER_DIR)/FrameworkSource/IORunner.o
OBJ_PATHS += $(OBJ_FILES)

$(COMPILER_DIR)/patterndata.o: $(CMSIS_DSP_TESTDIR)/patterndata.c
	@mkdir -p $(@D)
	$(CC) -c $(OPTIMIZATIONS) $(CFLAGS) -std=c11 $(DEFINES) $(APP_INCLUDES) $< -o $@

$(COMPILER_DIR)/%.o: $(CMSIS_DSP_TESTDIR)/%.cpp
	@mkdir -p $(@D)
	$(CPP) -c $(OPTIMIZATIONS) $(CFLAGS) -std=c++11 -fno-rtti $(DEFINES) $(APP_INCLUDES)  $< -o $@

# Special handling of this file which was modified from the CMSIS-DSP package
$(COMPILER_DIR)/FrameworkSource/IORunner.o: ../../../benchmark-package/Applications/cmsis-common/IORunner.cpp
	@mkdir -p $(@D)
	$(CPP) -c $(OPTIMIZATIONS) $(CFLAGS) -std=c++11 -fno-rtti $(DEFINES) $(APP_INCLUDES)   $< -o $@

#########################################################################################
# Required for dependencies to work correctly when parameters change
#########################################################################################

# The target application needs to depend on the DSP library
$(TARGET_APP): $(COMPILER_DIR)/libCMSISDSP.a

$(BUILD_DIR)/$(TARGET_UNMERGED): $(COMPILER_DIR)/libCMSISDSP.a $(OBJ_FILES)

# Make each object file dependent on the system configuration application parameters.
$(OBJ_FILES): $(TARGET_PAR)
