/*
** Copyright (c) 2022-2024 by Arm Limited (or its affiliates). All Rights Reserved.
*/

#include "serial.h"
#include <stdio.h>

void fpu_initalize() {
// Don't enable the FPU if we pass in the below flag
// This happens when we turn off FP instructions in the compiler
#ifndef NO_FPU
#endif
}
