#*********************************************************************
#      Copyright (c) 2022-2024 by Arm Limited (or its affiliates). All Rights Reserved.
#*********************************************************************
SYSTEM_APP := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
include $(SYSTEM_APP)/makefile/makefile-shared.inc
## Shared flags between compilers
SERIAL_DEFINES =
# The name of the CPU in the CMSIS Device directory
CMSIS_DEVICE = ARMCM3
# Specify which CPU header file we want to include used to compile CMSIS app.
DEFINES      += -D $(CMSIS_DEVICE) -D CMSDK_UART
############################################ AC6
ifeq ($(COMPILER),AC6)
CPU_CFLAGS   = -mcpu=cortex-m3 -mthumb --target=arm-arm-none-eabi
CPU_LDFLAGS  = --cpu=Cortex-M3 --entry=ResetHandler
CPU_ASFLAGS  = --cpu=Cortex-M3 -mthumb
DIS_UTIL_FLAGS += --cpu=Cortex-M3
############################################ GCC-RM
else ifeq ($(COMPILER),GCC)
CPU_CFLAGS   = -mcpu=cortex-m3 -mthumb
CPU_LDFLAGS  = -mcpu=cortex-m3 --entry=ResetHandler -u _printf_float
CPU_ASFLAGS  = -mcpu=cortex-m3 -mthumb
DIS_UTIL_FLAGS += -marmv7
else
$(error ERROR: COMPILER variable not valid. Must be either AC6 or GCC. Currently: $(COMPILER) )
endif
########################################################################################
# Startup code to initalize processor
$(COMPILER_DIR)/startup_M.o: $(SYSTEM_APP)/startup/startup_M.c
	$(CC) -c $(OPTIMIZATIONS) $(CFLAGS) $(DEFINES) $< -o $@
# PMU code to count specific benchmark sections
$(COMPILER_DIR)/pmu_M.o: $(SYSTEM_APP)/pmu-and-marker/pmu_M.c
	$(CC) -c $(OPTIMIZATIONS) $(CFLAGS) $(DEFINES) -I $(SYSTEM_APP)/compilers $< -o $@
OBJ_FILES += $(COMPILER_DIR)/startup_M.o $(COMPILER_DIR)/pmu_M.o
