Copyright 2023 Arm Limited (or its affiliates). All rights reserved.
=======================================================================================
Description
=======================================================================================
This README describes the supported compilers and applications for this system, along
with memory map(s) of the system.
For details on how to use this information, please see the top level README
=======================================================================================
Applications And Compilers Supported By This System
=======================================================================================
The names and identifiers of this system and it's supported applications and compilers
are shown below in the format 'name (identifier)'. The names are for informational
purposes only, please use the identifiers in all cases.
Cortex-R82 (r82x1-axi4)
    Applications:
        hello (hello)
        dhrystone (dhrystone)
        coremark (coremark)
        whetstone (whetstone)
        matrix-add (matrix-add)
    Compilers:
        GCC, Version 11.3.rel1 (GCC aarch64-none-elf-gcc)
        AC6, Arm Compiler 6.19

=======================================================================================
Memory Maps
=======================================================================================
A simplified memory map for this system with no Tightly Coupled Memories:
Peripheral                        Start address         End address
    Main memory                       0x00000000            0x7FFFFFFF
    Reserved                          0x80000000            0x9C08FFFF
    User software output (Uart)       0x9C090000            0x9C09FFFF
    Reserved                          0x9C0A0000            0x9C20FFFF
    User software exec tracker        0x9C210000            0x9C21FFFF
    Reserved                          0x9C220000            0xFFFFFFFF

A simplified memory map for this system with Tightly Coupled Memories enabled:
Peripheral                        Start address         End address
    ITCM                              0x00000000            (ITCMsize-1)
    Main memory                       ITCMsize              0x1FFFFFFF
    DTCM                              0x20000000            0x20000000+(DTCMsize-1)
    Main memory                       0x20000000+DTCMsize   0x2010403F
    Reserved                          0x20104040            0x9C08FFFF
    User software output (Uart)       0x9C090000            0x9C09FFFF
    Reserved                          0x9C0A0000            0x9C20FFFF
    User software exec tracker        0x9C210000            0x9C21FFFF
    Reserved                          0x9C220000            0xFFFFFFFF
