/*********************************************************************/
/*      Copyright (c) 2020-2023 Arm Limited. All rights reserved.    */
/*                                                                   */
/*                         IP Selection Sandbox                      */
/*********************************************************************/
/* General PMU functions */

#include <stdint.h>

// Use a reference CPU frequency for time-keeping if not provided (1MHz)
#ifndef CPU_FREQUENCY
#define CPU_FREQUENCY 1000000
#endif

#ifdef __cplusplus
extern "C" {
#endif
/**
 * Start the software marker, which begins tracking of execution statistics such as clock cycles,
 * cycles per instruction, and others. Marker results are only valid after this function
 * is called. Multiple calls to this function may result in undefined behavior.
*/
void start_marker(void);

/**
 * Stop the software marker, which stops tracking of execution statistics and prints them
 * to stdout. These execution statistics are no longer tracked after this call is made.
*/
void stop_marker(void);

/**
 * Resume the software marker from a previously-paused state. Software execution statistic tracking
 * resumes after this is called without resetting any values.
 * This only has an effect if pause_marker() has been called beforehand.
*/
void resume_marker(void);

/**
 * Pause the software marker. All software execution statistic tracking is paused, but can be
 * continued with a call to resume_marker(). The stop_marker() function can be called while
 * paused to finalize and print statistics.
*/
void pause_marker(void);

/**
 * Print the current software execution statistics, regardless of the current state (running, paused, stopped).
 * Note that calling this function without calling pause_marker() or stop_marker() first will likely result in
 * inconsistent results, as the print functions called by this function have a significant impact on execution
 * statistics. Values printed are only meaningful if start_marker() has been called first.
*/
void print_marker(void);

/**
 * Get the current cycle count of the marked software region, returning the number of clock cycles
 * that have passed during marked software tracking. Cycles during a paused state are not counted.
 * This value is only meaningful if start_marker() has been called first.
*/
unsigned get_marked_cycles(void);

/**
 * Get the current time passed of the marked software region, in seconds. This value is determined by
 * dividing the cycle count from get_marked_cycles() by the frequency of the software simulation, which
 * is defined by the preprocessor definition CPU_FREQUENCY. If CPU_FREQUENCY is not defined, it defaults
 * to 1000000 (1MHz). If CPU_FREQUENCY is defined and set to zero, this function always returns zero.
 */
float get_marked_seconds(void);

void __reset_cycle_counter();
void __start_cycle_counter();
void __stop_cycle_counter();
unsigned __get_cycle_count();
#ifdef __cplusplus
}
#endif
