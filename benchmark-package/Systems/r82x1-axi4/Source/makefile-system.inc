#*********************************************************************
#      Copyright (c) 2022-2023 by Arm Limited (or its affiliates). All Rights Reserved.
#*********************************************************************
SYSTEM_APP := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
include $(SYSTEM_APP)/makefile/makefile-shared.inc
############################################ AC6
ifeq ($(COMPILER),AC6)
CPU_CFLAGS   = -mcpu=cortex-r82  --target=aarch64-arm-none-eabi -mstrict-align
CPU_LDFLAGS  = --entry=bootcode --startup=bootcode
CPU_ASFLAGS  = -mcpu=cortex-r82
# Tell the disassembler to render the .dis file with the right instruction set for the CPU
DIS_UTIL_FLAGS += --cpu=8-R.64
############################################ GCC-RM
else ifeq ($(COMPILER),GCC)
CPU_CFLAGS  += -march=armv8-a+fp -mstrict-align
CPU_LDFLAGS += -march=armv8-a+fp --entry=bootcode
# Tell the disassembler to render the .dis file with the right instruction set for the CPU
DIS_UTIL_FLAGS += -marmv8-a
else
$(error ERROR: COMPILER variable not valid. Must be either AC6 or GCC. Currently: $(COMPILER) )
endif
########################################################################################
# Startup code to initalize processor
$(COMPILER_DIR)/startup_R82.o: $(SYSTEM_APP)/startup/startup_R82.s
	$(CC) -c -x assembler-with-cpp $(OPTIMIZATIONS) $(CFLAGS) -I $(SYSTEM_APP)/startup/startup_R82_includes $(DEFINES) $< -o $@
$(COMPILER_DIR)/startup_R82_vectors.o: $(SYSTEM_APP)/startup/startup_R82_vectors.s
	$(CC) -c -x assembler-with-cpp $(OPTIMIZATIONS) $(CFLAGS) -I $(SYSTEM_APP)/startup/startup_R82_includes $(DEFINES) $< -o $@
$(COMPILER_DIR)/startup_R82_stackheap.o: $(SYSTEM_APP)/startup/startup_R82_stackheap.s
	$(CC) -c -x assembler-with-cpp $(OPTIMIZATIONS) $(CFLAGS) -I $(SYSTEM_APP)/startup/startup_R82_includes $(DEFINES) $< -o $@
$(COMPILER_DIR)/pmu_Av8.o: $(SYSTEM_APP)/pmu-and-marker/pmu_Av8.s
	$(CC) -c -x assembler-with-cpp $(OPTIMIZATIONS) $(CFLAGS) $(DEFINES) $< -o $@
OBJ_FILES += $(COMPILER_DIR)/startup_R82.o $(COMPILER_DIR)/startup_R82_vectors.o $(COMPILER_DIR)/startup_R82_stackheap.o $(COMPILER_DIR)/pmu_Av8.o
