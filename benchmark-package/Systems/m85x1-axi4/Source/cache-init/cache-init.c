/*
** Copyright (c) 2019,2021 Arm Limited. All rights reserved.
*/

#include "serial.h"
#include <stdio.h>

// __builtin_arm_isb is not defined for gcc
#if defined(__GNUC__)
#define __ISB()
#else
#define __ISB() __builtin_arm_isb(0xF)
#endif

#define ICIALLU 0xE000EF50
#define CCR 0xE000ED14 /* configuration and control reg */
#define NSCCR 0xE002ED14 /* non secure configuration and control reg */
#define ACTLR 0xE000E008 /* auxiliary control reg */
void cache_initalize() {
/* Invalidate the caches */
  *(volatile unsigned int*)(ICIALLU) = 0;

  /* Enable loop/branch cache, branch prediction, both I & D caches */
  *(volatile unsigned int*)(CCR) |= 0xF0000;
  __asm volatile("dsb":::"memory");
  __asm volatile("isb":::"memory");

  /* These writes are not applicable for M7, ARMV7M */
  /* Enable non secure loop/branch cache, branch prediction, both I & D caches */
   *(volatile unsigned int*)(NSCCR) |= 0xF0000;
  __asm volatile("dsb":::"memory");
  __asm volatile("isb":::"memory");
  /* Enable Branch prediction using low overhead loops, clear bit 5 */
  /*  v8.1-M LOB bit */
    *(volatile unsigned int*)(ACTLR) &= 0xFFFFFFDFUL;
    __asm volatile("dsb":::"memory");
    __asm volatile("isb":::"memory");
  }
