Copyright 2023 Arm Limited (or its affiliates). All rights reserved.
=======================================================================================
Description
=======================================================================================
This README describes the supported compilers and applications for this system, along
with memory map(s) of the system.
For details on how to use this information, please see the top level README
=======================================================================================
Applications And Compilers Supported By This System
=======================================================================================
The names and identifiers of this system and it's supported applications and compilers
are shown below in the format 'name (identifier)'. The names are for informational
purposes only, please use the identifiers in all cases.
Cortex-M33 (m33x1-axi4)
    Applications:
        hello (hello)
        dhrystone (dhrystone)
        coremark (coremark)
        whetstone (whetstone)
        matrix-add (matrix-add)
        cmsis-dsp-BasicMaths-F32 (cmsis-dsp-basicmaths-f32)
        cmsis-dsp-BasicMaths-Q31 (cmsis-dsp-basicmaths-q31)
        cmsis-dsp-Transform-F32 (cmsis-dsp-transform-f32)
        cmsis-dsp-Transform-Q31 (cmsis-dsp-transform-q31)
    Compilers:
        GCC, Version 11.3.rel1 (GCC gcc_arm_none_eabi)
        AC6, Arm Compiler 6.19

=======================================================================================
Memory Maps
=======================================================================================
A simplified memory map for this system with no Tightly Coupled Memories:
Peripheral                        Start address         End address
    Code memory                       0x00000000            0x1FFFFFFF
    Data memory                       0x20000000            0x20081000
    Reserved                          0x40000000            0x401FFFFF
    User software output (Uart)       0x40200000            0x40200FFF
    Reserved                          0x40201000            0x402070EF
    User software exec tracker        0x402070F0            0x40207FFF
    Reserved                          0x40208000            0xFFFFFFFF
