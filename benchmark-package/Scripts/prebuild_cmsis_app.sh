#!/bin/bash
#
# Copyright 2022 ARM Limited.
# All rights reserved.
#
# Script to pre-build CMSIS-DSP Test applications for Cortex-M Platforms
#
#
echo on

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#Location of CMSIS-DSP Test directory
TESTDIR=$SCRIPT_DIR/../CMSIS/CMSIS-DSP-1.14.2/Testing

TESTNAME=$1

pushd "$TESTDIR" || exit

rm -f Output.pickle

python3 preprocess.py -f bench.txt
./createDefaultFolder.sh
python3 processTests.py -e
python3 processTests.py -e "$TESTNAME"

popd || exit
