This repository [https://gitlab.arm.com/ip-explorer/user-software] contains the ARM IP Explorer user-software.tgz
package contents. 

The user-software package serves as a template for creating custom applications which can be uploaded to IP Explorer.
This repository can be utilized to locally manage source code revisions and to easily obtain bug fixes, enhancements,
and updates published to the user-software package.

The files are tagged with a package version. If experiencing issues uploading custom applications to IP Explorer,
please check that your files are of the latest tagged version.  Guidance on how to use git to synchronize local
files with latest versions can be found at: https://git-scm.com/doc

For information on how use the user-software package to create and upload custom applications, please refer
to: benchmark-package/README.md

The package contents adhere to the license terms described in benchmark-package/license_terms/third_party_licenses.txt